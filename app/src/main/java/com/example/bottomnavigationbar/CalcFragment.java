package com.example.bottomnavigationbar;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.android.material.button.MaterialButton;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link CalcFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class CalcFragment extends Fragment implements View.OnClickListener {

    MaterialButton btn1, btn2, btn3, btn4, btn5, btn6, btn7, btn8, btn9, btn0, btnPlus, btnMinus, btnMultiply, btnDivide, btnEqual, btnClear;

    TextView tvResult, tvExpresion;



    public CalcFragment() {
        // Required empty public constructor
    }

    public static CalcFragment newInstance(String param1, String param2) {
        CalcFragment fragment = new CalcFragment();
        Bundle args = new Bundle();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_calc, container, false);
        btn0 = view.findViewById(R.id.button0);
        btn0.setOnClickListener(this);
        tvExpresion = view.findViewById(R.id.operacion);
        return view;
    }

    @Override
    public void onClick(View view) {

        //Extract the text from the button
        String text = ((MaterialButton) view).getText().toString();

        Log.d("TAG", "onClick: ");

        String expresion = tvExpresion.getText().toString();
        tvExpresion.setText(expresion+text);

    }
}