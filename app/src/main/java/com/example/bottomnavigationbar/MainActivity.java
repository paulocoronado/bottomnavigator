package com.example.bottomnavigationbar;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import android.content.ComponentName;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.FrameLayout;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.navigation.NavigationBarView;

public class MainActivity extends AppCompatActivity {

    FrameLayout frameLayout;
    Fragment fragment;

    BottomNavigationView bottomNavigationView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Initialize the frameLayout
        frameLayout = findViewById(R.id.frameLayout);

        // Initialize the fragment
        fragment = new CalcFragment();

        // Initialize the bottomNavigationView
        bottomNavigationView = findViewById(R.id.bottomNavigationView);

        //Add a listener to the bottomNavigationView
        bottomNavigationView.setOnItemSelectedListener(new NavigationBarView.OnItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                int id = item.getItemId();
                switch(id){
                    case R.id.itCalc:
                        //Add the fragment to the frameLayout
                        getSupportFragmentManager().beginTransaction().replace(R.id.frameLayout, new CalcFragment()).commit();
                        break;
                }
                return true;
            }
        });






    }
}